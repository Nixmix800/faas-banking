﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace reports_dotnet.Models
{
    public partial class Customer
    {
        public Customer()
        {
            TransactionCreditorNavigations = new HashSet<Transaction>();
            TransactionDebtorNavigations = new HashSet<Transaction>();
        }

        public string Iban { get; set; } = null!;
        public string Name { get; set; } = null!;
        public string? Address { get; set; }
        
        [NotMapped]
        public double Balance { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<Transaction> TransactionCreditorNavigations { get; set; }
        [JsonIgnore]
        public virtual ICollection<Transaction> TransactionDebtorNavigations { get; set; }
    }
}
