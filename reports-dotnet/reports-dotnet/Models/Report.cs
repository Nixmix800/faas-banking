﻿namespace reports_dotnet.Models;

public class Report
{
    public Customer? Customer { get; set; }
    public List<Transaction> Transactions { get; set; }
}