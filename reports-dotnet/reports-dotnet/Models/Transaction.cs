﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace reports_dotnet.Models
{
    public partial class Transaction
    {
        [Key]
        public int Id { get; set; }
        public DateTime ExecutionDate { get; set; }
        public string? Description { get; set; }
        [JsonIgnore]
        public string Debtor { get; set; } = null!;
        [JsonIgnore]
        public string Creditor { get; set; } = null!;
        
        public double Amount { get; set; }

        public virtual Customer CreditorNavigation { get; set; } = null!;

        public virtual Customer DebtorNavigation { get; set; } = null!;
    }
}
