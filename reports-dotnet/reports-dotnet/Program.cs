using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using reports_dotnet.DataAccess;
using Microsoft.EntityFrameworkCore.Design;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<Bank98Context>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("bank98Database")));
builder.Services.AddTransient<TransactionRepository>();
builder.Services.AddTransient<CustomerRepository>();

var app = builder.Build();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();