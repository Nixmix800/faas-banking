﻿using reports_dotnet.Models;

namespace reports_dotnet.DataAccess;

public class CustomerRepository
{
    private readonly Bank98Context _dbContext;
    private readonly ILogger<CustomerRepository> _logger;
    
    public CustomerRepository(Bank98Context dbContext, ILogger<CustomerRepository> logger)
    {
        _dbContext = dbContext;
        _logger = logger;
    }
    
    public Customer? GetAccountInfoForIban(string iban)
    {
        try
        {
            if (iban == string.Empty)
            {
                throw new ArgumentNullException($"Iban can not be null.");
            }
            var customer = _dbContext.Customers.FirstOrDefault(c => c.Iban == iban);
            
            if(customer != null)
                _logger.LogDebug($"Extracted Customer info for {iban}\n\tName: {customer.Name}\n\tAddress:{customer.Address}");
            else
                _logger.LogDebug($"Could not find any customerinfo for {iban}");
            
            return customer;
        }
        catch (Exception e)
        {
            _logger.LogError($"During dataaccess following exception occured:\n{e.Message}");
            throw;
        }
    }
}