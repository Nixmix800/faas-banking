﻿using Microsoft.EntityFrameworkCore;
using reports_dotnet.Models;

namespace reports_dotnet.DataAccess;

public class TransactionRepository
{
    private readonly Bank98Context _dbContext;
    private readonly ILogger<TransactionRepository> _logger;
    
    public TransactionRepository(Bank98Context dbContext, ILogger<TransactionRepository> logger)
    {
        _dbContext = dbContext;
        _logger = logger;
    }

    public List<Transaction> GetTransactionBeforeTimestamp(DateTime timestamp)
    {
        try
        {
            //if (timestamp == null)
            //{
            //    throw new ArgumentNullException($"Timestamp can not be null.");
            //}
            var transactions = _dbContext.Transactions.Where(t => t.ExecutionDate <= timestamp).ToList();
            _logger.LogDebug($"{transactions.Count} have been extracted that lie before {timestamp}");
            return transactions;
        }
        catch (Exception e)
        {
            _logger.LogError($"During dataaccess following exception occured:\n{e.Message}");
            throw;
        }
    }

    public IEnumerable<Transaction> GetTransactionBeforeTimestampWithIban(string iban, DateTime timestamp)
    {
        try
        {
            var transactions = _dbContext.Transactions.Where(t => (t.Creditor == iban || t.Debtor == iban) && t.ExecutionDate <= timestamp).ToList();
            _logger.LogDebug($"Transactions for {iban} have been extracted.\n{transactions.Count} have been extracted that lie before {timestamp}");
            return transactions;
        }
        catch (Exception e)
        {
            _logger.LogError($"During dataaccess following exception occured:\n{e.Message}");
            throw;
        }
    }

    public IEnumerable<Transaction> GetTransactionWithinMonthAndYear(string iban, int year, int month)
    {
        try
        {
            var transactions = _dbContext.Transactions.Where(t => (t.Creditor == iban || t.Debtor == iban) && (t.ExecutionDate.Month == month && t.ExecutionDate.Year == year)).ToList();
            _logger.LogDebug($"Transactions for {iban} have been extracted.\n{transactions.Count} have been extracted that are in the {month}. month");
            return transactions;
        }
        catch (Exception e)
        {
            _logger.LogError($"During dataaccess following exception occured:\n{e.Message}");
            throw;
        }
    }

    public double GetBalanceForIbanUntil(string iban, DateTime timestamp)
    {
        try
        {
            var balance = _dbContext.Transactions.Where(t =>
                (t.Creditor == iban || t.Debtor == iban) && t.ExecutionDate <= timestamp).Select(transactions => transactions.Amount).Sum();
            _logger.LogDebug($"Transactions for {iban} have been extracted.\nCustomer has: {balance} Geld in balance.");
            return balance;
        }
        catch (Exception e)
        {
            _logger.LogError($"During dataaccess following exception occured:\n{e.Message}");
            throw;
        }
    }
}