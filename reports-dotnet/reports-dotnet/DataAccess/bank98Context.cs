﻿using Microsoft.EntityFrameworkCore;
using reports_dotnet.Models;

namespace reports_dotnet.DataAccess
{
    public partial class Bank98Context : DbContext
    {
        public Bank98Context()
        {
        }

        public Bank98Context(DbContextOptions<Bank98Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Iban)
                    .HasName("customers_pk");

                entity.ToTable("customers", "bank-system");

                entity.HasIndex(e => e.Iban, "customers_iban_uindex")
                    .IsUnique();

                entity.Property(e => e.Iban)
                    .HasMaxLength(40)
                    .HasColumnName("iban");

                entity.Property(e => e.Address).HasMaxLength(254);

                entity.Property(e => e.Name).HasMaxLength(80);
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.ToTable("transactions", "bank-system");
                
                entity.Property(e => e.Id).HasColumnName("id");
                
                entity.HasIndex(e => e.Id, "transactions_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Creditor)
                    .HasColumnType("varchar")
                    .HasMaxLength(40);

                entity.Property(e => e.Debtor)
                    .HasColumnType("varchar")
                    .HasMaxLength(40);

                entity.Property(e => e.Description).HasMaxLength(254);

                entity.Property(e => e.ExecutionDate).HasColumnType("timestamp without time zone");

                entity.HasOne(t => t.DebtorNavigation)
                    .WithMany(c => c.TransactionDebtorNavigations)
                    .HasForeignKey(t => t.Debtor)
                    .HasConstraintName("transactions_customers_iban_fk");;
                
                entity.HasOne(t => t.CreditorNavigation)
                    .WithMany(c => c.TransactionCreditorNavigations)
                    .HasForeignKey(t => t.Creditor)
                    .HasConstraintName("transactions_customers_iban_fk_2");
                
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
