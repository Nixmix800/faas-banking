﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace reports_dotnet.Controllers
{
    /// <summary>
    /// </summary>
    [DataContract]
    public class Error
    {
        /// <summary>
        ///     The error message.
        /// </summary>
        /// <value>The error message.</value>
        [Required]
        [DataMember(Name = "errorMessage")]
        public string ErrorMessage { get; set; }
    }
}