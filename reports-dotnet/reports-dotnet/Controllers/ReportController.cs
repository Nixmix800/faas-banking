using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reports_dotnet.DataAccess;
using reports_dotnet.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace reports_dotnet.Controllers;

[ApiController]
[Route("[controller]")]
public class ReportController : ControllerBase
{
    private readonly ILogger<ReportController> _logger;
    private readonly TransactionRepository _transactionRepository;
    private readonly CustomerRepository _customerRepository;

    public ReportController(ILogger<ReportController> logger, TransactionRepository transactionRepository, CustomerRepository customerRepository)
    {
        _logger = logger;
        _transactionRepository = transactionRepository;
        _customerRepository = customerRepository;
    }
    
    [HttpGet]
    [Route("/report/{iban}/{timestamp}")]
    [SwaggerOperation("GetReportForIBAN")]
    [SwaggerResponse(statusCode: 200, type: typeof(Transaction), description: "Successful response")]
    [SwaggerResponse(statusCode: 400, type: typeof(Error), description: "An error occurred loading.")]
    [SwaggerResponse(statusCode: 204, type: typeof(Error), description: "No transactions registered yet.")]
    public IActionResult Get([FromRoute] [Required] [RegularExpression("^[A-Z]{2}(?:[ ]?[0-9]){18,20}$")] string iban, [FromRoute] DateTime timestamp)
    {
        var transactions = _transactionRepository.GetTransactionBeforeTimestampWithIban(iban, timestamp).ToList();
        var customer = _customerRepository.GetAccountInfoForIban(iban);
        if (customer != null)
        {
            var report = new Report
            {
                Customer = customer,
                Transactions = transactions
            };
            return Ok(report);
        }
        else
        {
            return NotFound("No customer with this IBAN found.");
        }
    }
    
    [HttpGet]
    [Route("/report/month/{iban}/{year}/{month}")]
    [SwaggerOperation("GetReportForIBAN")]
    [SwaggerResponse(statusCode: 200, type: typeof(Transaction), description: "Successful response")]
    [SwaggerResponse(statusCode: 400, type: typeof(Error), description: "An error occurred loading.")]
    [SwaggerResponse(statusCode: 204, type: typeof(Error), description: "No transactions registered yet.")]
    public IActionResult Get([FromRoute] [Required] [RegularExpression("^[A-Z]{2}(?:[ ]?[0-9]){18,20}$")] string iban, [FromRoute] int year, [FromRoute] [RegularExpression("^([1-9]|[0-1][0-2])$")] int month)
    {
        var transactions = _transactionRepository.GetTransactionWithinMonthAndYear(iban, year, month).ToList();
        var customer = _customerRepository.GetAccountInfoForIban(iban);
            
        var now = DateTime.Now;
        var timestamp = new DateTime(year, month, 31);
        
        if (customer != null)
        {
            customer.Balance = _transactionRepository.GetBalanceForIbanUntil(iban, timestamp);
            var report = new Report
            {
                Customer = customer,
                Transactions = transactions
            };
            return Ok(report);
        }
        else
        {
            return NotFound("No customer with this IBAN found.");
        }
    }

    [HttpGet]
    [Route("/report/timestamp/{timestamp}")]
    [SwaggerOperation("GetReportForIBAN")]
    [SwaggerResponse(statusCode: 200, type: typeof(Transaction), description: "Successful response")]
    [SwaggerResponse(statusCode: 400, type: typeof(Error), description: "An error occurred loading.")]
    [SwaggerResponse(statusCode: 204, type: typeof(Error), description: "No transactions registered yet.")]
    public IActionResult Get([FromRoute] DateTime timestamp)
    {
        var report = new Models.Report
        {
            Customer = new Models.Customer()
            {
                Name = "Harald",
                Address = "Irgendwas 1",
                Iban = "AT102011141243394288"
            },
            Transactions = _transactionRepository.GetTransactionBeforeTimestamp(timestamp)
        };
        return Ok(report);
    }
}
