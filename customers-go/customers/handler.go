package function

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/gddo/httputil/header"
	_ "github.com/lib/pq"
	_ "github.com/openfaas-incubator/go-function-sdk"
	"io"
	"net/http"
	"os"
	"strings"
)

const (
	host     = "localhost"
	port     = "5432"
	user     = "postgres"
	password = "Abcd1234"
	dbname   = "bank98"
)

type Customer struct {
	Name    string
	Address string
	IBAN    string
}

// db pool shared between function calls
var db *sql.DB

func init() {
	var err error

	//handle readout of environment vars
	databaseHost, ok := os.LookupEnv("DATABASE_HOST")
	if !ok {
		fmt.Println("DATABASE_HOST is not present")
		databaseHost = host
	} else {
		fmt.Printf("Database-Host: %s\n", databaseHost)
	}

	databasePort, ok := os.LookupEnv("DATABASE_PORT")
	if !ok {
		fmt.Println("DATABASE_PORT is not present")
		databasePort = port
	} else {
		fmt.Printf("Database-Port: %s\n", databasePort)
	}

	databaseUser, ok := os.LookupEnv("DATABASE_USER")
	if !ok {
		fmt.Println("DATABASE_USER is not present")
		databaseUser = user
	} else {
		fmt.Printf("Database-User: %s\n", databaseUser)
	}

	databasePwd, ok := os.LookupEnv("DATABASE_PWD")
	if !ok {
		fmt.Println("DATABASE_PWD is not present")
		databasePwd = password
	} else {
		fmt.Printf("Database-Pwd: %s\n", databasePwd)
	}

	databaseName, ok := os.LookupEnv("DATABASE_NAME")
	if !ok {
		fmt.Println("DATABASE_NAME is not present")
		databaseName = dbname
	} else {
		fmt.Printf("Database-Name: %s\n", databaseName)
	}

	//initialize db connection
	psqlconn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		databaseHost, databasePort, databaseUser, databasePwd, databaseName)

	db, err = sql.Open("postgres", psqlconn)

	//check if there was an error during database connection
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("Connection to database established")
	}
}

// Handle is called every time a request is made
func Handle(w http.ResponseWriter, req *http.Request) {
	//construct prepared statement
	fmt.Println("Hello from handler")
	query := `insert into "bank98"."bank-system"."customers" ("iban", "Name", "Address") values($1, $2, $3)`
	var customer Customer
	ctx := req.Context()

	if req.Body != nil {
		defer req.Body.Close()
		fmt.Println("Decoding now")
		err := decodeJSONBody(w, req, &customer)
		if err != nil {
			var mr *malformedRequest
			if errors.As(err, &mr) {
				fmt.Println(err.Error())
				fmt.Println(mr.msg)
				http.Error(w, "malformed Request: "+mr.msg, mr.status)
			} else {
				fmt.Println(err.Error())
				http.Error(w, "Error while decoding JSON", http.StatusInternalServerError)
			}
			return
		}
		fmt.Printf("Customer: %+v\n", customer.Name)
	}

	//check if customer is not null
	if (Customer{}) != customer {
		//check if required fields exist
		if customer.IBAN != "" && customer.Name != "" {
			fmt.Println("Execution Query now:")
			//fill query and execute

			_, err := db.ExecContext(ctx, query, customer.IBAN, customer.Name, customer.Address)

			if err != nil {
				fmt.Println("Error during database communication.")
				fmt.Println(err.Error())

				if strings.Contains(err.Error(), "violates unique contraint") {
					fmt.Println("IBAN already taken")
					http.Error(w, "Could not insert record into database.\nIBAN already taken", http.StatusInternalServerError)
					return
				}
				http.Error(w, "Could not insert record into database.\n"+err.Error(), http.StatusInternalServerError)
			} else {
				// write result
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				err := json.NewEncoder(w).Encode(customer)
				if err != nil {
					fmt.Println(err.Error())
					http.Error(w, "Error during JSON encoding.\n"+err.Error(), http.StatusInternalServerError)
				}
			}

		} else {
			http.Error(w, "Customer fields IBAN and Name must be set!", http.StatusBadRequest)
		}
	} else {
		http.Error(w, "Customer can not be empty", http.StatusBadRequest)
	}
}

type malformedRequest struct {
	status int
	msg    string
}

func (mr *malformedRequest) Error() string {
	return mr.msg
}

func decodeJSONBody(w http.ResponseWriter, r *http.Request, dst interface{}) error {
	if r.Header.Get("Content-Type") != "" {
		value, _ := header.ParseValueAndParams(r.Header, "Content-Type")
		if value != "application/json" {
			msg := "Content-Type header is not application/json"
			return &malformedRequest{status: http.StatusUnsupportedMediaType, msg: msg}
		}
	}

	r.Body = http.MaxBytesReader(w, r.Body, 1048576)

	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	err := dec.Decode(&dst)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError

		switch {
		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case errors.Is(err, io.ErrUnexpectedEOF):
			msg := fmt.Sprintf("Request body contains badly-formed JSON")
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			msg := fmt.Sprintf("Request body contains unknown field %s", fieldName)
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case errors.Is(err, io.EOF):
			msg := "Request body must not be empty"
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case err.Error() == "http: request body too large":
			msg := "Request body must not be larger than 1MB"
			return &malformedRequest{status: http.StatusRequestEntityTooLarge, msg: msg}

		default:
			return err
		}
	}

	err = dec.Decode(&struct{}{})
	if err != io.EOF {
		msg := "Request body must only contain a single JSON object"
		return &malformedRequest{status: http.StatusBadRequest, msg: msg}
	}

	return nil
}
