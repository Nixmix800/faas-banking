module faas.com/transactions

go 1.17

require (
	github.com/golang/gddo v0.0.0-20210115222349-20d68f94ee1f
	github.com/lib/pq v1.10.4
	github.com/openfaas-incubator/go-function-sdk v0.0.0-20200405082418-b31e65bf8a33
)
